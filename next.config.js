const withImages = require('next-images');
const isProduction = process.env.NODE_ENV === 'production';

module.exports = withImages({
  reactStrictMode: true,
  assetPrefix: isProduction ? 'https://mochamadhalim.gitlab.io/PMS-Project/' : '',
});
